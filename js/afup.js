/**
 * Registration of a service worker
 */

// Make sure service workers are supported
if ('serviceWorker' in navigator) {

    console.log('YAY! Service workers are supported!');

    // On load, try to register it...
    window.addEventListener('load', () => {

        // Standard JavaScript object navigator and method serviceWorker
        navigator.serviceWorker
            .register('../sw_cached_pages.js')
            .then(reg => console.log('Service Worker: Registered (Pages)'))
            .catch(err => console.log(`Service Worker: Error: ${err}`));
    });
}
