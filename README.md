# AFUP PWA Demo 2019

The project used for demoing at AFUP, in 2019.

Original sources by Brad Traversy, checkout at https://www.traversymedia.com/

Thanks you Brad for providing a REALLY simple and efficient tutorial on PWA and starting to work with caches.

## Understanding the goal of this demo

- Registering a service worker
- Caching static assets
- Cache the whole app 

Use browser's tool to make it offline/online and see how the browser tools for SW handle the caches.

Switch between service workers registration in afup.js. 
On has specific registry (like, excluding the image).
The other one caches everything.

Just then shot down the web server and refresh. If you've browsed once, you'll hit the cache without going out.